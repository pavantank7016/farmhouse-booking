import React, { useEffect, useState, useContext } from 'react';

import {
  ApolloClient, ApolloLink, ApolloProvider, HttpLink, InMemoryCache,
} from '@apollo/client';

import {firebase} from './firebase';

const httpLink = new HttpLink({ uri: process.env.NEXT_PUBLIC_API_URL });
const middlewareAuthLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      'x-hasura-role': 'admin_user',
    },
  });
  return forward(operation);
});
const httpLinkWithAuthToken = middlewareAuthLink.concat(httpLink);
export const clientTemp = new ApolloClient({
  ssrMode: typeof window === 'undefined', // set to true for SSR
  link: httpLinkWithAuthToken,
  cache: new InMemoryCache({
    addTypename: false,
    resultCaching: false,
  }),
});


export const ApolloComponent = ({ pageProps, Component }) => {
  const [client, setClient] = useState(clientTemp);
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
};
