import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import { useLazyQuery } from '@apollo/client';
// 
import styles from 'styles/login.module.scss';
import { GET_USER } from 'queries/user';
import { manageisUserLogin } from 'src/utils';

const Login = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorText, setErrorText] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [getUser, { data, loading }] = useLazyQuery(GET_USER);

  const checkLogin = async () => {
    const isLogin = await manageisUserLogin('get');
    if (isLogin) {
      router.push('/admin/dashboard');
    }
  }

  useEffect(() => {
    checkLogin();
  }, [])

  const onSubmit = async () => {
    console.log('logg')
    setIsLoading(true);
    getUser({
      variables: {
        email: email,
        password: password
      }
    }).then((res) => {
      setIsLoading(false);
      if (res.data.user?.length > 0) {
        manageisUserLogin('set')
        router.push('/admin/dashboard');
      } else {
        setErrorText('Email or Password incorrect');
      }
    }).catch((e) => {
      setErrorText(e.message);
      setIsLoading(false);
    })

  }

  return (
    <div className={styles.mainContainer}>
      <img src='/image/farmhous-2.jpg' />
      <div className={styles.card}>
        <div className={styles.name}>
          <span className={styles.design}>L</span>ogin
        </div>
        <input
          onChange={(e) => setEmail(e.target.value)}
          className={styles.textbox}
          type="text"
          placeholder="Email Address"
        />
        <input
          onChange={(e) => setPassword(e.target.value)}
          className={styles.textbox}
          type="password"
          placeholder="Password"
        />
        <span className={styles.error}>{errorText}</span>
        <button className={styles.login} disabled={email.trim() == "" && password.trim() == ""} onClick={onSubmit} >
          <a className={styles.farmLogin}> {isLoading ? 'Loading...' : 'Sign In'}</a>
        </button>
      </div>
    </div>
  );
}

export default Login;
