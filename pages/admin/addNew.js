import { useRef, useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import styles from 'styles/AddNew.module.scss';
import { manageisUserLogin } from 'src/utils';
import { firebase } from 'lib/firebase';
import { INSERT_FARM } from 'queries/farms';
import { useMutation } from '@apollo/client';

const AddNew = () => {
  const router = useRouter();
  const mainFileInputRef = useRef(null)
  const otherFileInputRef = useRef(null)

  const [mainImageUrl, setMainImageUrl] = useState('')
  const [farmname, setFarmName] = useState('')
  const [address, setAddress] = useState('')
  const [rentMonMorning, setRentMonMorning] = useState('')
  const [rentSunMorning, setRentSunMorning] = useState('')
  const [rentMonNight, setRentMonNight] = useState('')
  const [rentSunNight, setRentSunNight] = useState('')
  const [totalPerson, setTotalPerson] = useState('')
  const [extraPersonCharge, setExtraPersonCharge] = useState('')
  const [sqft, setSqFt] = useState('')
  const [poolSqft, setPoolSqFt] = useState('')
  const [locationLink, setLocationLink] = useState('')
  const [otherImageUrl, setOtherImageUrl] = useState([])
  const [filesUpload, setFileUploads] = useState([])
  const [isLoading, setLoading] = useState(false)
  const [assets, setAssets] = useState({
    kitchen: '',
    bathroom: '',
    bed: '',
    khatla: '',
    gadla: ''
  })

  const [addFarm, { data, loading }] = useMutation(INSERT_FARM);
  const [selections, setSelections] = useState([]);
  const options = [
    'સ્વિમિંગ પૂલ',
    'ફ્રીઝ',
    'બાળકો નો સ્વિમિંગ પૂલ',
    'ખુરશી',
    'સોફા સેટ',
    'હોલ એ.સી',
    'મ્યુઝિક સિસ્ટમ',
    'ટી.વી',
    'બેડરૂમ એ.સી',
    'કાર પાર્કિંગ',
    'ગઝેબો',
    'સ્ટ્રીટ લાઈટ',
    'ગાર્ડન',
    'ટેરેસ',
    'બીજી સગવડો',
    'ગાદલા',
    'ખાટલા',
    'ચિલ્ડ્રેન પ્લે એરિયા',
  ]

  const checkLogin = async () => {
    const isLogin = await manageisUserLogin('get');
    if (!isLogin) {
      router.push('/admin/login');
    }
  }

  useEffect(() => {
    checkLogin();
  }, [])

  const handleCheckboxChange = (key) => {
    let sel = [...selections]
    let find = sel.indexOf(key)
    if (find > -1) {
      sel.splice(find, 1)
    } else {
      sel.push(key)
    }

    setSelections(sel)
  }

  const uploadImageAsPromise = (imageFile) => {
    return new Promise((resolve, reject) => {
      var storageRef = firebase.storage().ref(`/farms/${imageFile.name}`);
      var task = storageRef.put(imageFile);
      //Update progress bar
      task.on('state_changed',
        (snapshot) => {
          var percentage = snapshot.bytesTransferred / snapshot.totalBytes * 100;
        },
        (err) => {
          console.log(err);
        },
        () => {
          storageRef.getDownloadURL().then(url => {
            resolve(url)
          })
        }
      );
    });
  }

  const uploadSingle = () => {
    return new Promise((resolve, reject) => {
      var storageRef = firebase.storage().ref(`/farms/${mainImageUrl.name}`);
      var task = storageRef.put(mainImageUrl);
      //Update progress bar
      task.on('state_changed',
        (snapshot) => {
          var percentage = snapshot.bytesTransferred / snapshot.totalBytes * 100;
        },
        (err) => {
          console.log(err);
        },
        () => {
          storageRef.getDownloadURL().then(url => {
            resolve(url)
          })
        }
      );
    });
  }

  const handleSave = async () => {
    setLoading(true);
    Promise.all(filesUpload.map((file) => uploadImageAsPromise(file))).then(async (value) => {
      const mainUrl = await uploadSingle();
      addFarm({
        variables: {
          farmname: farmname,
          address: address,
          rent: {
            mon_fri_morning: rentMonMorning,
            mon_fri_night: rentMonNight,
            sun_sat_morning: rentSunMorning,
            sun_sat_night: rentSunNight
          },
          total_person_limit: totalPerson,
          extra_person_charge: extraPersonCharge,
          sq_ft: sqft,
          pool_sq_ft: poolSqft,
          amenities: selections,
          farm_photo: mainUrl,
          all_photos: value,
          assets: assets,
          location_link: locationLink
        }
      }).then(() => {
        setLoading(false);
        router.push('/admin/dashboard')
        setFileUploads([]);
      }).catch(() => {
        setLoading(false);
        alert('not working')
      })
    })
  }

  const handleOtherImage = (e) => {
    const selectedFIles = [];
    const filesUploads = [];
    const targetFiles = e.target.files;
    const targetFilesObject = [...targetFiles]
    targetFilesObject.map((file) => {
      filesUploads.push(file)
      return selectedFIles.push(URL.createObjectURL(file))
    })
    setFileUploads(filesUploads)
    setOtherImageUrl(selectedFIles);

  }

  const removeItem = (index) => {
    let tempArr = [...otherImageUrl];
    tempArr?.splice(index, 1)
    setOtherImageUrl(tempArr)
  }

  return (
    <div className={styles.mainContainer}>
      <div className={styles.innerContainer}>
        <span>Add New Farm</span>
        <div className={styles.form}>
          <TextField
            required
            id="standard-required"
            label="ફાર્મ નું નામ"
            variant="standard"
            value={farmname}
            onChange={(e) => setFarmName(e.target.value)}
          />
          <TextField
            required
            id="standard-required"
            label="સરનામું "
            variant="standard"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />

          <div className={styles.rents}>
            <fieldset className="scheduler-border">
              <legend className="scheduler-border">ભાડુ</legend>

              <TextField
                required
                id="standard-required"
                label="દિવસ(સોમ થી શુક્ર)"
                variant="standard"
                type="number"
                name="rent"
                value={rentMonMorning}
                onChange={(e) => setRentMonMorning(e.target.value)}
              />
              <TextField
                required
                id="standard-required"
                label="રાત્રી(સોમ થી શુક્ર)"
                variant="standard"
                type="number"
                name="rent"
                value={rentMonNight}
                onChange={(e) => setRentMonNight(e.target.value)}
              />
              <TextField
                required
                id="standard-required"
                label="દિવસ(શનિ-રવિ)"
                variant="standard"
                type="number"
                name="rent"
                value={rentSunMorning}
                onChange={(e) => setRentSunMorning(e.target.value)}
              />
              <TextField
                required
                id="standard-required"
                label="રાત્રી(શનિ-રવિ)"
                variant="standard"
                type="number"
                name="rent"
                value={rentSunNight}
                onChange={(e) => setRentSunNight(e.target.value)}
              />
            </fieldset>
          </div>

          <TextField
            id="standard-required"
            className={styles.spacingInput}
            label="કિચન"
            variant="outlined"
            type="number"
            onChange={(e) => setAssets({ ...assets, kitchen: e.target.value })} value={assets.kitchen}
          />

          <TextField
            id="standard-required"
            className={styles.spacingInput}
            label="બાથરૂમ"
            variant="outlined"
            type="number"
            onChange={(e) => setAssets({ ...assets, bathroom: e.target.value })} value={assets.bathroom}
          />

          <TextField
            id="standard-required"
            className={styles.spacingInput}
            label="બેડ"
            variant="outlined"
            type="number"
            onChange={(e) => setAssets({ ...assets, bed: e.target.value })} value={assets.bed}
          />

          <TextField
            id="standard-required"
            className={styles.spacingInput}
            label="ખાટલા"
            variant="outlined"
            type="number"
            onChange={(e) => setAssets({ ...assets, khatla: e.target.value })} value={assets.khatla}
          />

          <TextField
            id="standard-required"
            className={styles.spacingInput}
            label="ગાદલા"
            variant="outlined"
            type="number"
            onChange={(e) => setAssets({ ...assets, gadla: e.target.value })} value={assets.gadla}
          />

          <div className={styles.farmCapacity}>
            <TextField id="standard-required" label="કુલ ક્ષમતા(વ્યક્તિ મા):" variant="outlined" type="number"
              onChange={(e) => setTotalPerson(e.target.value)} value={totalPerson} />
            <TextField id="standard-required" label="વધારાનો ચાર્જ (વ્યક્તિ દીઠ):" variant="outlined" type="number"
              onChange={(e) => setExtraPersonCharge(e.target.value)} value={extraPersonCharge} />
          </div>
          <div className={styles.farmSize}>
            <TextField id="standard-required" label="ફાર્મનું કદ" variant="outlined" type="number"
              onChange={(e) => setSqFt(e.target.value)} value={sqft} />
            <TextField id="standard-required" label="સ્વિમિંગ પૂલનું કદ" variant="outlined" type="number"
              onChange={(e) => setPoolSqFt(e.target.value)} value={poolSqft} />
          </div>
          <div className={styles.features}>
            <fieldset className="scheduler-border">
              <legend className="scheduler-border">વિશેષતા</legend>
              {options.map((option) => (
                <div style={{marginBottom: '10px'}} key={option}>
                  <input
                    type="checkbox"
                    checked={selections.includes(option)}
                    onChange={() => handleCheckboxChange(option)}
                  />
                  <span>{option}</span>
                </div>
              ))}
            </fieldset>
          </div>

          <TextField
            id="standard-required"
            style={{marginTop: '10px'}}
            label="location link"
            variant="outlined"
            onChange={(e) => setLocationLink(e.target.value)} value={assets.locationLink}
          />

          <div className={styles.mainImageContainer}>
            <span>ફાર્મ નો મુખ્ય  ફોટા</span>
            <input onChange={(e) => setMainImageUrl(e.target.files[0])} ref={mainFileInputRef} accept="image/*" type="file" hidden="true" />
            <span className={styles.mainButton} onClick={() => mainFileInputRef.current.click()} >
              {mainImageUrl ? 'Change' : 'Upload'}
            </span>
          </div>
          {mainImageUrl &&
            <img className={styles.mainImage} src={URL.createObjectURL(mainImageUrl)} alt="" />
          }
          <div className={styles.otherImageContainer}>
            <span>ફાર્મ ના અન્ય ફોટાઓ</span>
            <input onChange={(e) => handleOtherImage(e)} ref={otherFileInputRef} accept="image/*" multiple type="file" hidden="true" />
            <span className={styles.mainButton} onClick={() => otherFileInputRef.current.click()}>
              {otherImageUrl.length == 0 ? ' Upload' : 'Change'}
            </span>
          </div>
          {
            otherImageUrl?.map((otherImg, index) => {
              return (
                <div className={styles.imageContainer} key={index}>
                  <img className={styles.mainImage} src={otherImg} alt="" />
                  <div className={styles.closeIcon} onClick={() => removeItem(index)}>
                    <img src='/icons/close.svg' alt='' />
                  </div>
                </div>
              )
            })
          }

          <button
            onClick={handleSave}
            className={styles.saveButton}>
            {isLoading ? 'Loading...' : 'Save'}
          </button>
        </div>
      </div>
    </div>
  )
}

export default AddNew;