import styles from 'styles/Home.module.scss';
import { ImLocation } from 'react-icons/im';
import { useRouter } from 'next/router';
import { useLazyQuery } from '@apollo/client';
import { GET_FARMS } from 'queries/farms';
import { useEffect } from 'react';
import useLongPress from "src/useLongPress";

const Home = () => {
  const router = useRouter();
  const [getUser, { data, loading }] = useLazyQuery(GET_FARMS);

  useEffect(() => {
    getUser()
  }, [])

  const onCardClick = (item) => {
    router.push({
      pathname: '/details',
      query: {
        farmData: JSON.stringify(item)
      }
    }, '/details')
  }

  const onLongPress = () => {
    console.log('longpress is triggered');
    router.push('/admin')
  };

  const onClick = () => {
    console.log('click is triggered')
  }

  const defaultOptions = {
    shouldPreventDefault: true,
    delay: 500,
  };
  const longPressEvent = useLongPress(onLongPress, onClick, defaultOptions);

  return (
    <>
      <div className={styles.mainContainer}>
        <div className={styles.container}>
          <h1 className={styles.mainHeading} {...longPressEvent}>સુરત સાંઈ ફાર્મ</h1>
        </div>
        <div className={styles.row1}>
          <p className={styles.notice1}>
            નોંધ:દરેક તહેવાર ના ભાવ અલગ રહેશે. કોલ કરીને ભાવ જાણીલેવો. ફોન નંબર:<a href="tel:+91 79845 45484"> +91 79845 45484</a>
          </p>
        </div>
        {!loading &&
          data?.farms?.map((item, index) => {
            return (
              <div onClick={() => onCardClick(item)} key={index} className={styles.box1}>
                <img src={item.farm_photo} alt="logo" width="100%" className={styles.entryGet} />
                <h3 className={styles.farmName}>{item.title}</h3>
                <div className={styles.info}>
                  <div className={styles.column}>
                    <span className={styles.farmName}>{item.farmname}</span>
                    <span>{item.rent.mon_fri_morning} Per Day</span>
                  </div>
                  <div className={styles.location}>
                    <ImLocation className={styles.locationIcon} />
                    <span className={styles.city}>{item.address}</span>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    </>
  )
}

export default Home;