import 'styles/globals.scss'
import "swiper/css"
import "swiper/css/pagination";;
import { ApolloComponent } from 'lib/apolloclient';

function MyApp({ Component, pageProps }) {
  return <ApolloComponent pageProps={pageProps} Component={Component} />
}

export default MyApp
